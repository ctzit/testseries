namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnsweredBacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HasAnswerd = c.Boolean(nullable: false),
                        MarkedReview = c.Boolean(nullable: false),
                        ExamId = c.Int(nullable: false),
                        User = c.String(),
                        QuestionID = c.Guid(nullable: false),
                        AnswersID = c.Guid(nullable: false),
                        TestInProgress_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestInProgresses", t => t.TestInProgress_Id)
                .Index(t => t.TestInProgress_Id);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Ans = c.String(),
                        Description = c.String(),
                        Placement = c.Int(nullable: false),
                        IsCorrectAns = c.Boolean(nullable: false),
                        Exams = c.String(),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Query = c.String(),
                        SubjectId = c.Int(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        Description = c.String(),
                        UserId = c.String(),
                        Set_SetId = c.Guid(),
                        TestInProgress_Id = c.Guid(),
                        UnitOrChapter_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Sets", t => t.Set_SetId)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .ForeignKey("dbo.TestInProgresses", t => t.TestInProgress_Id)
                .ForeignKey("dbo.UnitOrChapters", t => t.UnitOrChapter_Id)
                .Index(t => t.SubjectId)
                .Index(t => t.Set_SetId)
                .Index(t => t.TestInProgress_Id)
                .Index(t => t.UnitOrChapter_Id);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExamSubjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExamId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        NoOfQuestion = c.Int(nullable: false),
                        Description = c.String(),
                        ShowName = c.String(),
                        Exams_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exams", t => t.Exams_Id)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId)
                .Index(t => t.Exams_Id);
            
            CreateTable(
                "dbo.Exams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Alias = c.String(),
                        Description = c.String(),
                        Details = c.String(),
                        Negative = c.Int(nullable: false),
                        OnNoQuestion = c.Int(nullable: false),
                        IsNegativeMarking = c.Boolean(nullable: false),
                        PercentageAllowed = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Duration = c.Int(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        Random = c.Boolean(nullable: false),
                        Tenant_TenantUrl = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.Tenant_TenantUrl)
                .Index(t => t.Tenant_TenantUrl);
            
            CreateTable(
                "dbo.Sets",
                c => new
                    {
                        SetId = c.Guid(nullable: false),
                        SetName = c.String(nullable: false, maxLength: 40),
                        Exam_Id = c.Int(),
                    })
                .PrimaryKey(t => t.SetId)
                .ForeignKey("dbo.Exams", t => t.Exam_Id)
                .Index(t => t.Exam_Id);
            
            CreateTable(
                "dbo.Tenants",
                c => new
                    {
                        TenantUrl = c.String(nullable: false, maxLength: 128),
                        TenantHost = c.String(),
                        AddedOn = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        ActivatedOn = c.DateTime(nullable: false),
                        ExpiresOn = c.DateTime(nullable: false),
                        AddedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TenantUrl)
                .ForeignKey("dbo.AspNetUsers", t => t.AddedBy_Id)
                .Index(t => t.AddedBy_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.EReports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        User = c.String(),
                        Subject = c.String(),
                        Correct = c.Int(nullable: false),
                        Worng = c.Int(nullable: false),
                        Attempted = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        InProgress = c.Guid(nullable: false),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ImageForSites",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Data = c.Binary(),
                        Encoding = c.String(),
                        User = c.String(),
                        Agent = c.String(),
                        On = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        UserUrl = c.String(),
                        UserHost = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Solutions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        Solved = c.String(),
                        Active = c.Boolean(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.TestInProgresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        User = c.String(),
                        TotalQ = c.Int(nullable: false),
                        RightA = c.Int(nullable: false),
                        ExamId = c.Int(nullable: false),
                        Over = c.Boolean(nullable: false),
                        IsPaused = c.Boolean(nullable: false),
                        TimeRemaining = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UnitOrChapters",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(maxLength: 200),
                        AddedOn = c.DateTime(nullable: false),
                        Subject_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subjects", t => t.Subject_Id)
                .Index(t => t.Subject_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnitOrChapters", "Subject_Id", "dbo.Subjects");
            DropForeignKey("dbo.Questions", "UnitOrChapter_Id", "dbo.UnitOrChapters");
            DropForeignKey("dbo.Questions", "TestInProgress_Id", "dbo.TestInProgresses");
            DropForeignKey("dbo.AnsweredBacks", "TestInProgress_Id", "dbo.TestInProgresses");
            DropForeignKey("dbo.Solutions", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Questions", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.ExamSubjects", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.ExamSubjects", "Exams_Id", "dbo.Exams");
            DropForeignKey("dbo.Exams", "Tenant_TenantUrl", "dbo.Tenants");
            DropForeignKey("dbo.Tenants", "AddedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Questions", "Set_SetId", "dbo.Sets");
            DropForeignKey("dbo.Sets", "Exam_Id", "dbo.Exams");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.UnitOrChapters", new[] { "Subject_Id" });
            DropIndex("dbo.Solutions", new[] { "QuestionId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Tenants", new[] { "AddedBy_Id" });
            DropIndex("dbo.Sets", new[] { "Exam_Id" });
            DropIndex("dbo.Exams", new[] { "Tenant_TenantUrl" });
            DropIndex("dbo.ExamSubjects", new[] { "Exams_Id" });
            DropIndex("dbo.ExamSubjects", new[] { "SubjectId" });
            DropIndex("dbo.Questions", new[] { "UnitOrChapter_Id" });
            DropIndex("dbo.Questions", new[] { "TestInProgress_Id" });
            DropIndex("dbo.Questions", new[] { "Set_SetId" });
            DropIndex("dbo.Questions", new[] { "SubjectId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropIndex("dbo.AnsweredBacks", new[] { "TestInProgress_Id" });
            DropTable("dbo.UnitOrChapters");
            DropTable("dbo.TestInProgresses");
            DropTable("dbo.Solutions");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ImageForSites");
            DropTable("dbo.EReports");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Tenants");
            DropTable("dbo.Sets");
            DropTable("dbo.Exams");
            DropTable("dbo.ExamSubjects");
            DropTable("dbo.Subjects");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
            DropTable("dbo.AnsweredBacks");
        }
    }
}
