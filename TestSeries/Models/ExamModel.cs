﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class ExamModel
    {
        public int ID { get; set; }
        public List<QuestionData> QuestionD { get; set; }
        public int Timer { get; set; }
        public List<ExQes> ExamQues { get; set; }
        public int No { set { value = QuestionD.Count(); }
            get { return QuestionD.Count(); }
        }

    }
    public class ExQes {
        public int id { get; set; }
        public string SubjectName  { get; set; }
        public string DisplayName { get; set; }
        public int Quantity { get; set; }
        public int ExamID { get; set; }
        public int SubjectId { get; set; }
    
    }

    public class QuestionData {
        public Guid QuestionID { get; set; }
        public int ExamId { get; set; }
        public Guid Ans { get; set; }
        public bool Answered { get; set; }
        public int Sub { get; set; }
        public QuestionData() {
            Answered = false;
        }

    }


    public class QuestionModel {
        public Guid ID { get; set; }
        public string Question { get; set; }
        public Answer[] Answer { get; set; }
        public int NoOfAns { get; set; }
        public int Type { get; set; }
    }
    public class Answer{
        public Guid Id { get; set; }
        public string Ans { get; set; }
        public AnsweredBack Answered { get; set; }
    }

    public class ExamReportModel
    {
        public Exams Exam { get; set; }
        public TestInProgress Ip { get; set; }
        public List<EReport> Ereport { get; set; }
        //public int MyProperty { get; set; }
        
    }

    public class Results {
       public Guid IPID { get; set; }
       public Exams Exam { get; set; }
       public List<string> Question { get; set; }
       public List<string> Ans { get; set; }
       public List<bool> Correct { get; set; }
       public List<bool> NotAttemp { get; set; }
       public Results() {
           Question = new List<string>();
           Ans = new List<string>();
           Correct = new List<bool>();
           NotAttemp = new List<bool>();
       }
    }
    public class ExamAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(
                            AuthorizationContext filterContext)
      {
          if (filterContext == null)
          {
              throw new ArgumentNullException( "filterContext" );
          }

          if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
          {string loginUrl = "";
              // get from cached variable from web configuration
          if (filterContext.RequestContext.HttpContext.Request.IsLocal)
              loginUrl = "http://localhost:1855/signup/logon/";
          else
              loginUrl = "http://justyousay.com/signup/logon/";
              if (filterContext.HttpContext.Request != null)
              {
                  loginUrl += "?ReturnUrl=" + filterContext.HttpContext
                                                           .Request
                                                           .Url
                                                           .AbsoluteUri;
              }

              filterContext.Result = new RedirectResult( loginUrl );
          }
      }
    }
}