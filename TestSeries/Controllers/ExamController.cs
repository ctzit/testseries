﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TestSeries.Models;

namespace TestSeries.Controllers
{
    public class ExamController : ApiController
    {
        private TestSeriesContext db = new TestSeriesContext();
        public ExamController()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
        }
        // GET: api/Exam
        public IQueryable<Exams> GetExams(int page=0,int size=20)
        {
            return db.Exams.OrderBy(i=>i.Id).Skip(page*size).Take(size);
        }
        [ResponseType(typeof(int))]
        [Route("api/exam/Records")]
        [HttpGet]
        public IHttpActionResult TotalRecord()
        {
           // Request.RequestUri.
            return Ok(db.Exams.Count());
        }
        // GET: api/Exam/5
        [ResponseType(typeof(Exams))]
        public IHttpActionResult GetExams(int id)
        {
            Exams exams = db.Exams.Find(id);
            if (exams == null)
            {
                return NotFound();
            }

            return Ok(exams);
        }

        // PUT: api/Exam/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExams(int id, Exams exams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exams.Id)
            {
                return BadRequest();
            }

            db.Entry(exams).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Exam
        [ResponseType(typeof(Exams))]
        public IHttpActionResult PostExams(Exams exams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Exams.Add(exams);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = exams.Id }, exams);
        }

        // DELETE: api/Exam/5
        [ResponseType(typeof(Exams))]
        public IHttpActionResult DeleteExams(int id)
        {
            Exams exams = db.Exams.Find(id);
            if (exams == null)
            {
                return NotFound();
            }

            db.Exams.Remove(exams);
            db.SaveChanges();

            return Ok(exams);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExamsExists(int id)
        {
            return db.Exams.Count(e => e.Id == id) > 0;
        }
    }
}