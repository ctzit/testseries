﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class UnitChapterViewModel 
    {
        public Guid Id { get; set; }

        public virtual Subject Subject { get; set; }
        [Display(Name = @"Unit/Chapter Name"), MinLength(3), MaxLength(200)]
        public string Name { get; set; }
        public DateTime AddedOn { get; set; }
        public int SubjectId { get; set; }
    }
}