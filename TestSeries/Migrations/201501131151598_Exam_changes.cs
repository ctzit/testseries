namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Exam_changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "SetCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exams", "SetCount");
        }
    }
}
