﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Areas.QuestionBank.Controllers
{
    public class AnswersController : Controller
    {
        private TestSeriesContext db = new TestSeriesContext();

        // GET: QuestionBank/Answers
        public async Task<ActionResult> Index()
        {
            var answers = db.Answers.Include(a => a.Query);
            return View(await answers.ToListAsync());
        }

        // GET: QuestionBank/Answers/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answers answers = await db.Answers.FindAsync(id);
            if (answers == null)
            {
                return HttpNotFound();
            }
            return View(answers);
        }

        // GET: QuestionBank/Answers/Create
        public ActionResult Create()
        {
            ViewBag.QuestionId = new SelectList(db.Questions, "ID", "Query");
            return View();
        }

        // POST: QuestionBank/Answers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,QuestionId,Ans,Description,Placement,IsCorrectAns,Exams,Active,AddedOn")] Answers answers)
        {
            if (ModelState.IsValid)
            {
                answers.Id = Guid.NewGuid();
                db.Answers.Add(answers);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.QuestionId = new SelectList(db.Questions, "ID", "Query", answers.QuestionId);
            return View(answers);
        }

        // GET: QuestionBank/Answers/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answers answers = await db.Answers.FindAsync(id);
            if (answers == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "ID", "Query", answers.QuestionId);
            return View(answers);
        }

        // POST: QuestionBank/Answers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,QuestionId,Ans,Description,Placement,IsCorrectAns,Exams,Active,AddedOn")] Answers answers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answers).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "ID", "Query", answers.QuestionId);
            return View(answers);
        }

        // GET: QuestionBank/Answers/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answers answers = await db.Answers.FindAsync(id);
            if (answers == null)
            {
                return HttpNotFound();
            }
            return View(answers);
        }

        // POST: QuestionBank/Answers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Answers answers = await db.Answers.FindAsync(id);
            db.Answers.Remove(answers);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
