﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSeries.Areas.Student.Models;
using TestSeries.Infra;

namespace TestSeries.Areas.Student.Controllers
{
    public class ProfileController : App
    {
        // GET: Student/Profile
        public ActionResult Index()
        {
            return View(UserDetails);
        }
        public ActionResult Edit()
        {
            return View(new EditProfileModel
            {
                City = UserDetails.City,
                ExamOn = UserDetails.ExamOn,
                FullName = UserDetails.FullName,
                HasImage = (UserDetails.HasImage!=null)?UserDetails.HasImage:false,
                ImageUrl = UserDetails.HasImage ? "/student/Profile/Image" : "/",
                PreparingFor = UserDetails.PreparingFor,
                TutionInstitueName = UserDetails.TutionInstitueName,
                ContactNo=UserDetails.PhoneNumber

            });
        }
        [HttpPost]
        public ActionResult Edit(EditProfileModel Model) {

            if (ModelState.IsValid)
            {
                if (Model.Image != null)
                {
                    UserDetails.ProfileImage = new byte[Model.Image.ContentLength];
                    Model.Image.InputStream.Read(UserDetails.ProfileImage, 0, Model.Image.ContentLength);
                    UserDetails.HasImage = true;
                }
                UserDetails.FullName = Model.FullName;
                UserDetails.City = Model.City;
                UserDetails.ExamOn = Model.ExamOn;
                //UserDetails.PhoneNumber = Model.ContactNo;   //Commented Due to login issue.
                UserDetails.PreparingFor = Model.PreparingFor;
                UserDetails.TutionInstitueName = Model.TutionInstitueName;
                dc.Entry(UserDetails).State = EntityState.Modified;
                dc.SaveChanges();
                Sucess("Profile Saved.");
                return Redirect("Index");
            }
            else
                return View(Model);
        }

        public ActionResult Image()
        {
            return new FileContentResult(UserDetails.ProfileImage,"img/jpg");
        }

        public ActionResult Mini()
        {
            return PartialView(UserDetails);
        }
    }
}