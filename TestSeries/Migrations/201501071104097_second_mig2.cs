namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second_mig2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "QuestionType", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "QuestionLevel", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "QuestionWeightage", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "Solution", c => c.String());
            AddColumn("dbo.Questions", "TimeBounded", c => c.Boolean(nullable: false));
            AddColumn("dbo.Questions", "TimeBound", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "Tenant_TenantUrl", c => c.String(maxLength: 128));
            CreateIndex("dbo.Questions", "Tenant_TenantUrl");
            AddForeignKey("dbo.Questions", "Tenant_TenantUrl", "dbo.Tenants", "TenantUrl");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "Tenant_TenantUrl", "dbo.Tenants");
            DropIndex("dbo.Questions", new[] { "Tenant_TenantUrl" });
            DropColumn("dbo.Questions", "Tenant_TenantUrl");
            DropColumn("dbo.Questions", "TimeBound");
            DropColumn("dbo.Questions", "TimeBounded");
            DropColumn("dbo.Questions", "Solution");
            DropColumn("dbo.Questions", "QuestionWeightage");
            DropColumn("dbo.Questions", "QuestionLevel");
            DropColumn("dbo.Questions", "QuestionType");
        }
    }
}
