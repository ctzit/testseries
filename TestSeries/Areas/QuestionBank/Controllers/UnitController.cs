﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Areas.QuestionBank.Controllers
{
    public class UnitController : Controller
    {
        private TestSeriesContext db = new TestSeriesContext();

        // GET: /QuestionBank/Unit/
        public async Task<ActionResult> Index()
        {
            return View(await db.UnitOrChapter.ToListAsync());
        }

        // GET: /QuestionBank/Unit/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnitOrChapter unitorchapter = await db.UnitOrChapter.FindAsync(id);
            if (unitorchapter == null)
            {
                return HttpNotFound();
            }
            return View(unitorchapter);
        }

        // GET: /QuestionBank/Unit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /QuestionBank/Unit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="Id,Name,AddedOn")] UnitOrChapter unitorchapter)
        {
            if (ModelState.IsValid)
            {
                unitorchapter.Id = Guid.NewGuid();
                db.UnitOrChapter.Add(unitorchapter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(unitorchapter);
        }

        // GET: /QuestionBank/Unit/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnitOrChapter unitorchapter = await db.UnitOrChapter.FindAsync(id);
            if (unitorchapter == null)
            {
                return HttpNotFound();
            }
            return View(unitorchapter);
        }

        // POST: /QuestionBank/Unit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="Id,Name,AddedOn")] UnitOrChapter unitorchapter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unitorchapter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(unitorchapter);
        }

        // GET: /QuestionBank/Unit/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnitOrChapter unitorchapter = await db.UnitOrChapter.FindAsync(id);
            if (unitorchapter == null)
            {
                return HttpNotFound();
            }
            return View(unitorchapter);
        }

        // POST: /QuestionBank/Unit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            UnitOrChapter unitorchapter = await db.UnitOrChapter.FindAsync(id);
            db.UnitOrChapter.Remove(unitorchapter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
