﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class Tenant
    {
        [Key]
        public string TenantUrl { get; set; }
        public string TenantHost { get; set; }
        public DateTime AddedOn { get; set; }
        public bool Active { get; set; }
        public DateTime ActivatedOn { get; set; }
        public DateTime ExpiresOn { get; set; }
        public virtual User AddedBy { get; set; }

    }
}