namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second_mig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AspNetUsers", "TutionInstitueName", c => c.String(maxLength: 400));
            AddColumn("dbo.AspNetUsers", "PreparingFor", c => c.String(maxLength: 100));
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
            AddColumn("dbo.AspNetUsers", "ExamOn", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.AspNetUsers", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.AspNetUsers", "ExamOn");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "PreparingFor");
            DropColumn("dbo.AspNetUsers", "TutionInstitueName");
            DropColumn("dbo.AspNetUsers", "FullName");
        }
    }
}
