﻿angular.module("ExamEntry", ["ngSanitize","ui.bootstrap"])
.factory("ExamFactory", ["$http", function ($http) {
    return {
        getExams: function (Pager,callback, errorCallback) {
            $http.get("/api/exam/?page="+Pager.page+"&size="+Pager.size).success(callback).error(errorCallback);
        },
        saveExam: function (Exam,callback, errorCallback) {
            $http.put("/api/exam/"+Exam.Id, Exam).success(callback).error(errorCallback);
        },
        getExam: function (Id,callback, errorCallback) {
            $http.get('/api/exam/' + Id).success(callback).error(errorCallback);
        },
        addExam: function (Exam,callback, errorCallback) {
            $http.post('/api/exam/', Exam).success(callback).error(errorCallback);
        },
        getRecords: function (callback, errorCallback) {
            $http.get('/api/exam/Records').success(callback).error(errorCallback)
        }
    }
}])
.controller("ExamController", ["$scope", "ExamFactory", function ($scope, ExamFactory) {
    $scope.Busy = true;
    $scope.IsNew = true;
    $scope.Pager = { page: 0, size: 10, TotalPage: 10,hasNext:false,hasPrev:false, TotalRecords:10 };
    $scope.Exam = {
        Id:0,
        Name: 'Name',
        Alias: 'Display Name',
        Description: 'Some short description.',
        TotalQuestion:150,
        Negative: 0,
        OnNoQuestion: 0,
        IsNegativeMarking: false,
        PercentageAllowed: false,
        Active: false,
        Duration: 180,
        SetCount: 3,
        AddedOn: new Date(),
        Random: false,
        Sets: null,
        Tenant:null
    };
    $scope.Exams =null;
    $scope.EditExam = function (exam) {
        $scope.Exam = exam;
        $scope.IsNew = false;
    }
    $scope.NewExam = function () {
        $scope.Exam = NewExam();
        $scope.IsNew = true;
    }
    $scope.init = function (Id) {
        $scope.Exams = [];
        ExamFactory.getRecords(Records, Error);
        if (Id == 0) {
            $scope.Busy = true;
            $scope.Exam = NewExam();
            $scope.IsNew = true;
            ExamFactory.getExams($scope.Pager, SetExams, Error);
        }
        else {
            $scope.Busy = true;
            $scope.IsNew = false;
            ExamFactory.getExams($scope.Pager, SetExams, Error);
            ExamFactory.getExam(Id, SetExam, Error);
            $scope.Busy = false;
        }
    }
    $scope.LoadExams = function () {
        ExamFactory.getExams($scope.Pager, SetExams, Error);
    }
    $scope.setPage = function (pageNo) {
        $scope.Pager.page = pageNo;
        ExamFactory.getExams($scope.Pager, SetExams, Error);
    };
    function NewExam() {
        return {
            Id: 0,
            Name: 'Name',
            Alias: 'Display Name',
            Description: 'Some short description.',
            TotalQuestion: 150,
            Negative: 0,
            OnNoQuestion: 0,
            IsNegativeMarking: false,
            PercentageAllowed: false,
            Active: false,
            Duration: 180,
            SetCount: 3,
            AddedOn: new Date(),
            Random: false,
            Sets: null,
            Tenant: null
        };
    }
   
    function SetExam(result){
        $scope.Exam=result;
        $scope.Busy=false;
    }
    function SetExams(result) {
        $scope.Exams=result,
        $scope.Busy = false;
    }
    function Error() {
        $scope.Busy = false;
    }
    function Records(r) {
        $scope.Pager.TotalRecords = r;
        $scope.Pager.TotalPage= Math.ceil($scope.Pager.TotalRecords / $scope.Pager.size);
    }
    $scope.SaveExam=function(flag) {
        $scope.Busy = true;
        $scope.Exam.Active = flag;
        if ($scope.IsNew) {
            $scope.IsNew = false;
            ExamFactory.addExam($scope.Exam, function (result) {
                $scope.Exam = result;
               
                ExamFactory.getRecords(Records, Error);
                ExamFactory.getExams($scope.Pager,SetExams, Error);
                $scope.Busy = false;
            }, Error);
        } else {
            ExamFactory.saveExam($scope.Exam, function (result) {
                //$scope.Exam = result;
                $scope.Busy = false;
            });
        }
    }


}]);