﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
namespace TestSeries.Models
{
    public class TestSeriesContext : IdentityDbContext<User>
    {
        public TestSeriesContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static TestSeriesContext Create()
        {
            return new TestSeriesContext();
        }
        

        public DbSet<Exams> Exams { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Solution> Solutions { get; set; }

        public DbSet<Answers> Answers { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<ExamSubjects> ExamSubjects { get; set; }
        /// <summary>
        /// Ansewer given by Student.
        /// </summary>
        public DbSet<AnsweredBack> AnsweredBack { get; set; }
        /// <summary>
        /// Test In Progress
        /// </summary>
        public DbSet<TestInProgress> TestInProgress { get; set; }

        public DbSet<EReport> EReports { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        
        public DbSet<ImageForSite> ImageForSites { get; set; }
        public DbSet<UnitOrChapter> UnitOrChapter { get; set; }

        
    }
}
