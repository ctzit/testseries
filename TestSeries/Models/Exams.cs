﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class Exams {
        public int Id { get; set; }
        [Display(Name="Exam Name")]
        public string Name { get; set; }
        [Display(Name="Display Name")]
        public string Alias { get; set; }
        [AllowHtml,
        Display(Name="Short Description")]
        public string Description { get; set; }
        //public string Details { get; set; }
        public int TotalQuestion { get; set; }
        [Display(Name="Number to Deduct.")]
        public int Negative { get; set; }
        [Display(Name="On Number of Question.")]
        public int OnNoQuestion { get; set; }
        [Display(Name="Is negative marking allowed.")]
        public bool IsNegativeMarking { get; set; }
        [Display(Name="Should floor the value?")]
        public bool PercentageAllowed { get; set; }
        //public virtual ICollection<Category> Categories { get; set; }
        [Display(Name="Is Published")]
        public bool Active { get; set; }
        [Display(Name="Duration of test in Minutes.")]
        public int Duration { get; set; }
        [Display(Name="Number of sets.")]
        public int SetCount { get; set; }
        [Display(Name="Recored Added On:"),DisplayFormat(DataFormatString="dd-MMM-yyyy")]
        public DateTime AddedOn { get; set; }
       // public int TotalQuestion { get; set; }
        public Exams() {
            if (AddedOn == null)
            {
                AddedOn = DateTime.UtcNow;
            }
        }
        public virtual ICollection<Set> Sets { get; set; }
        [Display(Name="Random Question(Auto Set) *Not recommended")]
        public bool Random { get; set; }
        public virtual Tenant Tenant { get; set; }

    }
   
}