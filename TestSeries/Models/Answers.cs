﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class Answers
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public virtual Question Query { get; set; }
        [AllowHtml]
        public string Ans { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int Placement { get; set; }
        public bool IsCorrectAns { get; set; }
        public string Exams { get; set; }
        public bool Active { get; set; }
        public DateTime AddedOn { get; set; }
        public Answers()
        {
            if (Id == null)
            {
                Id = Guid.NewGuid();
            }
            if (AddedOn == null)
            {
                AddedOn = DateTime.UtcNow;
            }
        }
    }
}