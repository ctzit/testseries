namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionmigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Questions", "UnitOrChapter_Id", "dbo.UnitOrChapters");
            DropIndex("dbo.Questions", new[] { "UnitOrChapter_Id" });
            CreateTable(
                "dbo.UnitOrChapterQuestions",
                c => new
                    {
                        UnitOrChapter_Id = c.Guid(nullable: false),
                        Question_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UnitOrChapter_Id, t.Question_ID })
                .ForeignKey("dbo.UnitOrChapters", t => t.UnitOrChapter_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_ID, cascadeDelete: true)
                .Index(t => t.UnitOrChapter_Id)
                .Index(t => t.Question_ID);
            
            DropColumn("dbo.Questions", "UnitOrChapter_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "UnitOrChapter_Id", c => c.Guid());
            DropForeignKey("dbo.UnitOrChapterQuestions", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.UnitOrChapterQuestions", "UnitOrChapter_Id", "dbo.UnitOrChapters");
            DropIndex("dbo.UnitOrChapterQuestions", new[] { "Question_ID" });
            DropIndex("dbo.UnitOrChapterQuestions", new[] { "UnitOrChapter_Id" });
            DropTable("dbo.UnitOrChapterQuestions");
            CreateIndex("dbo.Questions", "UnitOrChapter_Id");
            AddForeignKey("dbo.Questions", "UnitOrChapter_Id", "dbo.UnitOrChapters", "Id");
        }
    }
}
