﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Areas.QuestionBank.Controllers
{
    public class QuestionsController : Controller
    {
        private TestSeriesContext db = new TestSeriesContext();

        // GET: QuestionBank/Questions
        public async Task<ActionResult> Index()
        {
            var questions = db.Questions.Include(q => q.Subject);
            return View(await questions.ToListAsync());
        }

        // GET: QuestionBank/Questions/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // GET: QuestionBank/Questions/Create
        public ActionResult Create()
        {
            //ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name");
           Question q = new Question();
           ViewBag.IsNew = true;
            return View(q);
        }

        // POST: QuestionBank/Questions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Query,SubjectId,AddedOn,Description,UserId,QuestionType,QuestionLevel,QuestionWeightage,Solution,TimeBounded,TimeBound")] Question question)
        {
            if (ModelState.IsValid)
            {
                question.ID = Guid.NewGuid();
                db.Questions.Add(question);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", question.SubjectId);
            return View(question);
        }

        // GET: QuestionBank/Questions/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            ViewBag.IsNew = false;
            //ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", question.SubjectId);
            return View("Create", question);
        }

        // POST: QuestionBank/Questions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Query,SubjectId,AddedOn,Description,UserId,QuestionType,QuestionLevel,QuestionWeightage,Solution,TimeBounded,TimeBound")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", question.SubjectId);
            return View(question);
        }

        // GET: QuestionBank/Questions/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: QuestionBank/Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Question question = await db.Questions.FindAsync(id);
            db.Questions.Remove(question);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
