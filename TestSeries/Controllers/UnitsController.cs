﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TestSeries.Models;

namespace TestSeries.Controllers
{
    public class UnitsController : ApiController
    {
        private TestSeriesContext db = new TestSeriesContext();
        public UnitsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/Units
        
        public IQueryable<UnitOrChapter> GetUnitOrChapter(int Id)
        { var ret =
             db.UnitOrChapter.Where(i=>i.Subject.Id==Id);
        foreach (var item in ret)
        {
            item.Questions = new List<Question>();
        }
        return ret;
        }
        //[Route("UNITS/{Id}")]
        //public IQueryable<UnitOrChapter> GetUnitsOfSubject(int Id)
        //{
        //    return db.UnitOrChapter.Where(i=>i.Subject.Id==Id);
        //}

        // GET: api/Units/5
        ////[ResponseType(typeof(UnitOrChapter))]
        ////public IHttpActionResult GetUnitOrChapter(Guid id)
        ////{
        ////    UnitOrChapter unitOrChapter = db.UnitOrChapter.Find(id);
        ////    if (unitOrChapter == null)
        ////    {
        ////        return NotFound();
        ////    }

        ////    return Ok(unitOrChapter);
        ////}

        // PUT: api/Units/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUnitOrChapter(Guid id, UnitOrChapter unitOrChapter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != unitOrChapter.Id)
            {
                return BadRequest();
            }

            db.Entry(unitOrChapter).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnitOrChapterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Units
        [ResponseType(typeof(UnitOrChapter))]
        public IHttpActionResult PostUnitOrChapter(UnitChapterViewModel unitOrChapter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ui = new UnitOrChapter { Name = unitOrChapter.Name, AddedOn = DateTime.UtcNow, Id = Guid.NewGuid() };
            unitOrChapter.Id = Guid.NewGuid();
            Subject sub = db.Subjects.Find(unitOrChapter.SubjectId);
            if (sub == null)
            {
                ModelState.AddModelError("Name", "Subject No Found");
                return BadRequest(ModelState);
            }
            ui.Subject = sub;
            db.UnitOrChapter.Add(ui);
            
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UnitOrChapterExists(unitOrChapter.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = unitOrChapter.Id }, unitOrChapter);
        }

        // DELETE: api/Units/5
        [ResponseType(typeof(UnitOrChapter))]
        public IHttpActionResult DeleteUnitOrChapter(Guid id)
        {
            UnitOrChapter unitOrChapter = db.UnitOrChapter.Find(id);
            if (unitOrChapter == null)
            {
                return NotFound();
            }

            db.UnitOrChapter.Remove(unitOrChapter);
            db.SaveChanges();

            return Ok(unitOrChapter);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UnitOrChapterExists(Guid id)
        {
            return db.UnitOrChapter.Count(e => e.Id == id) > 0;
        }
    }
}