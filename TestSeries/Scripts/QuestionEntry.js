/**************************************************
 * @property GoldPi,CTZIT. 
 * @author:Imran Ali and Md Yusuf Alam
 * @version :1.0.0
 * Contact: imran@guitarguru.in
 *************************************************/
angular.module("Question", ["ui.bootstrap", "ngSanitize", "ngAnimate"])
.constant("Version","1.0.0")
.directive('ckEditor', function () {
 return {
        restrict: 'A', // only activate on element attribute
        scope: false,
        require: 'ngModel',
        controller: function ($scope, $element, $attrs) { }, //open for now
        link: function ($scope, element, attr, ngModel, ngModelCtrl) {
            if (!ngModel) return;
            if (attr.ckEditor === false)
                return;// do nothing if no ng-model you might want to remove this
            element.bind('click', function () {
                for (name in CKEDITOR.instances)
                    CKEDITOR.instances[name].destroy();
                var ck = CKEDITOR.replace(element[0]);
                ck.on('pasteState', function () {
                    $scope.$apply(function () {
                        ngModel.$setViewValue(ck.getData());
                    });
                });

                ngModel.$render = function (value) {

                    ck.setData(ngModel.$modelValue);
                };
            });
        }
    }
})
.factory('subjectFactory',["$http", function ($http) {
    return {
        getSubject: function (callback,errorCallback) {
           $http.get('/api/sub').success(callback).error(errorCallback);
        },
        saveSubject: function (subject,callback,errorCallback) {
            $http.post('/api/sub', subject).success(callback).error(errorCallback);
        },
        getUnits: function (id, callback,errorCallback) {
            $http.get('/api/units/'+id).success(callback).error(errorCallback);
        },
        saveUnit: function (Unit, callback,errorCallback) {
            $http.post('/api/units',Unit).success(callback).error(errorCallback);
        },
        addQuestion: function (Question, callback,errorCallback) {
            $http.post('/api/Ques', Question).success(callback).error(errorCallback);
        },
        getQuestion: function (QId,callback,errorCallback) {
            $http.get('/api/Ques/' + QId).success(callback).error(errorCallback);
        },
        saveQuestion: function (Question, callback,errorCallback) {
            $http.put('/api/Ques/' + Question.ID, Question).success(callback).errorCallback(errorCallback);
        }
    }
}])
.filter('unitSelectedFilter', ["$filter", function ($filter) {
    return function (inputArray,secondArry) {
        if (secondArry == null) {
            return inputArray;
        } else {
            var data = inputArray;
            var p=secondArry;
            for (var c = 0; c < secondArry.length; c++) {
                for (var cc = 0; cc < data.length; cc++) {
                    if(data[cc].Id==p[c].Id)
                    {
                        data.splice(cc, 1);
                    }
                }
            }
            return data;
        }
    }
}])
.controller("QuestionController", ["$scope", "subjectFactory", "$modal", "$log", function ($scope, subjectFactory, $modal, $log) {
    $scope.Busy = false;
    //--------------------------------//
    $scope.IsNew = true;
    //--------------------------------//
    $scope.Message = "";
    $scope.DisplayUnits = [];
    //for Radio Question Type.
    $scope.QuestionType = [{ value:0, Label: "Multiple Choice Question" },
        { value: 1, Label: "Multiple Selection Question" },
        { value: 2, Label: "Yes or No" },
        { value: 3, Label: "True or False" }];
    //Placement
    $scope.OldVal = 0;
    $scope.Count = 1;
    $scope.Level = [{ value:0,Label: "BEGINNER" },
        { value:1,Label: "MODERATE" },
        { value: 2, Label: "HIGH" }];
    //Question Entity
    $scope.Question =
        {
            ID: NewGuid(),
            Query: "Type your question here?",
            SubjectId: null,
            AddedOn: new Date(),
            Answers: [],
            Description: '',
            UserId: '',
            Tenant: null,
            QuestionType: 0,
            QuestionLevel: 1,
            QuestionWeightage: 1,
            Solution: "Your Solution",
            TimeBounded: false,
            TimeBound: 60,
            IsImportant: false,
            UnitOrChapters: [],
            Published:false
        };
    //Subject 
    $scope.Sub = "";
    //Container Unit.
    $scope.Unit = "";
    //Container for Units
    $scope.Units = [];
    //Add an answer.
    function AddAnswer(label, zero,select) {
        if (zero)
        {
            $scope.Count = 0;
        } else
        {
            $scope.Count++;
        }
       
        var Ans = {
            Id: NewGuid(),
            QuestionId: $scope.Question.ID,
            Ans: label,
            Description: '',
            Placement: $scope.Count,
            IsCorrectAns: select,
            Exams: '',
            Active: true,
            AddedOn: new Date()
        };
        $scope.Question.Answers.push(Ans)

    }
    // Bug #6 https://bitbucket.org/ctzit/testseries/issue/6/ckeditor-issue-in-question-entry
    function FixCkEditor() {
        for (name in CKEDITOR.instances)
            CKEDITOR.instances[name].destroy();
    }
    $scope.AddAnswer = function () {
        AddAnswer("Option", false, false);
    }
    //Removes Answers
    $scope.Remove = function (index) {
        FixCkEditor();
        $scope.Question.Answers.splice(index, 1);
    }
    //Toggle CkEditor
    $scope.Toggle = function (index) {
        var ckk = $scope.Answers[index].ckeditor;
        if (ckk) {
            $scope.Answers[index].ckeditor = false;
        } else {
            $scope.Answers[index].ckeditor = true;
        }

    }
    //List All Avaialbe Subject;
    $scope.Subjects = [];
    // Intialize
    $scope.init = function (user,newq,qid) {
        //Check New Question Or Old:
        
        if (!newq) {
            $scope.Busy = false;
            $scope.IsNew = false;
            subjectFactory.getQuestion(qid, function (result) {
                $scope.Question = result;
                $scope.OldVal = $scope.Question.QuestionType;
                subjectFactory.getSubject(function (subjects) {
                   // console.log(subjects);
                    $scope.Subjects = subjects;
                }, errorOccured);
                subjectFactory.getUnits($scope.Question.SubjectId, function (result) {
                    $scope.Units = result;
                    $scope.DisplayUnits=Display($scope.Units,$scope.Question.UnitOrChapters);
                }, errorOccured);
                $scope.Busy = true;
            });
        }
        else {
            $scope.Busy = false;
            $scope.Question.UserId = user;
            $scope.IsNew = true;
            //Bug #9 https://bitbucket.org/ctzit/testseries/issue/9/add-4-option-by-default-during-question
            AddAnswer('Option 1', true, true);
            AddAnswer('Option 2', false, false);
            AddAnswer('Option 3', false, false);
            AddAnswer('Option 4', false, false);

            subjectFactory.getSubject(function (subjects) {
               // console.log(subjects);
                $scope.Subjects = subjects;
            }, errorOccured);
            $scope.OldVal = $scope.Question.QuestionType;
            $scope.Busy = true;
    }  
    }
    //Load Units;
    $scope.LoadUnits = function () {
        $scope.Busy = false;
       // console.log($scope.Question.SubjectId)
       // $scope.Question.SubjectId = $scope.Question.Subject.Id;
        if ($scope.Question.SubjectId != null) {
            subjectFactory.getUnits($scope.Question.SubjectId, function (result) {
                $scope.Units = result;
                $scope.DisplayUnits=Display($scope.Units,$scope.Question.UnitOrChapters);
                $scope.Busy = true;
            }, errorOccured);
        }
    }
    //Invoke Subect entry Ui
    $scope.AddSubjectUi = function () {
        $scope.Sub = { Id: 0, Name: "Name Of Subject!", Description: "" };
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalSubjectInstanceController',
            resolve: {
                sub: function () {
                    return $scope.Sub;
                }
            }
        });


        modalInstance.result.then(function (selectedItem) {
            $scope.Sub = selectedItem;
            $scope.Busy = false;
            subjectFactory.saveSubject($scope.Sub, function () {
                $scope.Busy = false;
                 subjectFactory.getSubject(function (subjects) {
                    console.log(subjects);
                    $scope.Subjects = subjects;
                    $scope.Busy = true;
                }, errorOccured);
                }, errorOccured);
                $scope.Busy = true;
        }, function () {
          //  console.log("closed")
        });
    }
    //To check the change in question type.
    $scope.ChangeQT = function () {
        FixCkEditor();
        if (confirm("Are you sure")) {
           
            $scope.OldVal = $scope.Question.QuestionType;
            if ($scope.OldVal == 2) {
                $scope.Question.Answers = [];
                AddAnswer('Yes', true, true);
                AddAnswer('No', false, false);
            }
            if ($scope.OldVal == 3) {
                $scope.Question.Answers = [];
                AddAnswer('True', true, true);
                AddAnswer('False', false, false);
            }
        } else {
            $scope.Question.QuestionType = $scope.OldVal;
        }
    }
    
    // Invokes Modal for Unit Entry for selected Subject.
    $scope.AddUnitUi = function () {
        $scope.Unit = { Id: NewGuid(), Name: "Name Of Subject!", AddedOn: new Date(), SubjectId: $scope.Question.SubjectId };
        var modalInstance = $modal.open({
            templateUrl: 'UnitModalContent.html',
            controller: 'ModalInstanceController',
            resolve: {
                unit: function () {
                    return $scope.Unit;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.Unit = selectedItem;
            $scope.Busy = false;
            subjectFactory.saveUnit($scope.Unit, function () {
                $scope.Busy = false;
                //Bug fix #7 https://bitbucket.org/ctzit/testseries/issue/7/question-units-entry-bug
                 subjectFactory.getUnits($scope.Question.SubjectId, function (units) {
                     $scope.Units = units;
                     $scope.DisplayUnits=Display($scope.Unit,$scope.Question.UnitOrChapters);
                     $scope.Busy = true;
                 }, errorOccured);
            }, errorOccured);
        }, function () {
            
        });
    }
    
    //Adds Units to selectedUnits.
    $scope.AddToUnits = function (unit) {
       // console.log(unit)
        if (angular.isUndefined($scope.Question.UnitOrChapters))
            $scope.Question.UnitOrChapters = [];
        $scope.Question.UnitOrChapters.push(unit);
        
        $scope.DisplayUnits=Display($scope.Units,$scope.Question.UnitOrChapters);
    }
    //Select Question Type
    $scope.SelectQT = function (item) {
        $scope.SelectedQT = item;  
    }
    //Removes the units.
    $scope.RemoveFromUnits = function (index) {
        $scope.Question.UnitOrChapters.splice(index, 1);
        $scope.DisplayUnits=Display($scope.Units,$scope.Question.UnitOrChapters);
    }
    //Event to save the question to database
    $scope.SaveQuestion = function (published) {
        $scope.Busy = false;
        $scope.Question.Published = published;
        if ($scope.IsNew) {
             subjectFactory.addQuestion($scope.Question, function (result) {
                $scope.IsNew = false;
                $scope.Message = "Saved to Database!"
                $scope.Busy = true;
            },errorOccured);
        } else {
            subjectFactory.saveQuestion($scope.Question, function (result) {
                $scope.Message = "Saved to Database!"
                $scope.Busy = true;
            }, errorOccured)
        }
    }
    //Hack for radio select answers
    $scope.SelectThis = function (index) {
        for (var i=0; i < $scope.Question.Answers.length; i++)
            $scope.Question.Answers[i].IsCorrectAns = false;
        $scope.Question.Answers[index].IsCorrectAns = true;
    }
    //Bug #5 https://bitbucket.org/ctzit/testseries/issue/5/angular-array-filter-bug
    //Due to refrenced object filter was behaving Unsual.
    function Display(inputArray, secondArry) {
        var data = [];
        for (var i = 0; i < inputArray.length; i++)
            data.push(inputArray[i]);
        if (secondArry == null) {
            return data;
        } else {
           
            var p = secondArry;
            for (var c = 0; c < secondArry.length; c++) {
                for (var cc = 0; cc < data.length; cc++) {
                    if (data[cc].Id == p[c].Id) {
                        data.splice(cc, 1);
                    }
                }
            }
            return data;
        }
    }
    //function to genrate guid!  taken from stackover.com user groof!
    function NewGuid() {
       return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8); 
            return v.toString(16);
        });
    }
    function errorOccured() {
        $scope.Busy = true;
    }
    //--------------------------------//
}])
.controller('ModalSubjectInstanceController', ["$scope", "$modalInstance","sub", function ($scope, $modalInstance,sub) {

    $scope.Sub=sub;
    $scope.ok = function () {
        $modalInstance.close($scope.Sub);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}])
.controller('ModalInstanceController', ["$scope", "$modalInstance","unit", function ($scope, $modalInstance,unit) {

    $scope.Unit=unit;
    $scope.ok = function () {
        $modalInstance.close($scope.Unit);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);
