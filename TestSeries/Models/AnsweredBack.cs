﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class AnsweredBack
    {

        public int Id { get; set; }
        public bool HasAnswerd { get; set; }
        public bool MarkedReview { get; set; }
        public int ExamId { get; set; }
        public string User { get; set; }
        public Guid QuestionID { get; set; }
        public Guid AnswersID { get; set; }

    }
}