﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Controllers
{
    public class ImageController : Controller
    {
        TestSeriesContext dc = new TestSeriesContext();
        // GET: Image
        //public ActionResult Index()
        //{
        //    return View();
        //}
        /// <summary>
        /// Images Upload from CK Editor
        /// </summary>
        /// <param name="upload"></param>
        /// <param name="CKEditorFuncNum"></param>
        /// <param name="CKEditor"></param>
        /// <param name="langCode"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileWrapper upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string url;
            ImageForSite ck = new ImageForSite();
            if (ModelState.IsValid)
            {
                if (upload != null)
                {
                    ck.Data = new Byte[upload.ContentLength];
                    ck.Encoding = upload.ContentType ?? "image/jpeg";
                    upload.InputStream.Read(ck.Data, 0, upload.ContentLength);
                    if (User.Identity.IsAuthenticated)
                    {
                        ck.User = User.Identity.Name;
                    }
                    else
                    {
                        ck.User = "Not Known";
                    }
                    ck.Agent = Request.UserAgent;

                    dc.ImageForSites.Add(ck);
                    dc.SaveChanges();

                }

            }
            url = "/image/?id=" + ck.Id.ToString();
            string output = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + url + "\");</script></body></html>";
            return Content(output);
        }
        /// <summary>
        /// CK editor Images
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public FileContentResult Index(Guid Id)
        {
            ImageForSite prod = dc.ImageForSites.FirstOrDefault(p => p.Id == Id);
            if (prod != null)
            {
                return File(prod.Data, "image/png");
            }
            else
            {
                return null;
            }
        }
    }
}