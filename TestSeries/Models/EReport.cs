﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class EReport
    {
        public Guid Id { get; set; }
        public string User { get; set; }
        public string Subject { get; set; }
        public int Correct { get; set; }
        public int Worng { get; set; }
        public int Attempted { get; set; }
        public int Total { get; set; }
        public Guid InProgress { get; set; }
        public DateTime Time { get; set; }

    }
}