﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class UserModel
    {

        public Guid Username { get; set; }
        public string Name { get; set; }
        public int Question { get; set; }
        public bool Contiued { get; set; }
        public string Email { get; set; }
    }
    
}