﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Areas.Setter.Controllers
{
    public class ExamController : Controller
    {
        private TestSeriesContext db = new TestSeriesContext();


        public ActionResult Subjects()
        {
            return View();
        }

        // GET: /Setter/Exam/
        public async Task<ActionResult> Index()
        {
            return View(await db.Exams.ToListAsync());
        }

        // GET: /Setter/Exam/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exams exams = await db.Exams.FindAsync(id);
            if (exams == null)
            {
                return HttpNotFound();
            }
            return View(exams);
        }

        // GET: /Setter/Exam/Create
        public ActionResult Create()
        {
            return View(new Exams());
        }

        // POST: /Setter/Exam/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="Id,Name,Alias,Description,Details,Negative,OnNoQuestion,IsNegativeMarking,PercentageAllowed,Active,Duration,AddedOn,Random")] Exams exams)
        {
            if (ModelState.IsValid)
            {
                db.Exams.Add(exams);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(exams);
        }

        // GET: /Setter/Exam/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exams exams = await db.Exams.FindAsync(id);
            if (exams == null)
            {
                return HttpNotFound();
            }
            return View("Create",exams);
        }

        // POST: /Setter/Exam/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="Id,Name,Alias,Description,Details,Negative,OnNoQuestion,IsNegativeMarking,PercentageAllowed,Active,Duration,AddedOn,Random")] Exams exams)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exams).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(exams);
        }

        // GET: /Setter/Exam/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exams exams = await db.Exams.FindAsync(id);
            if (exams == null)
            {
                return HttpNotFound();
            }
            return View(exams);
        }

        // POST: /Setter/Exam/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Exams exams = await db.Exams.FindAsync(id);
            db.Exams.Remove(exams);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
