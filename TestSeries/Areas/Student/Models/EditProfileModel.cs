﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestSeries.Areas.Student.Models
{
    public class EditProfileModel
    {

        [MaxLength(100)]
        [Required]
        [Display(Name = "First Name")]
        public string FullName { get; set; }
        [MaxLength(400)]
        [Display(Name = "Current Institution Name")]
        public string TutionInstitueName { get; set; }
        [MaxLength(100)]
        [Required]
        [Display(Name = "Preapering For Exam?")]
        public string PreparingFor { get; set; }
        [Required]
        [Display(Name = "From ?")]
        public string City { get; set; }
        [Required]
        [Display(Name = "Exam On?")]
        public int ExamOn { get; set; }
        public HttpPostedFileWrapper Image { get; set; }
        public bool HasImage { get; set; }
        public string ContactNo { get; set; }
        public string ImageUrl { get; set; }

    }
}