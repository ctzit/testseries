﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class ExamReportView
    {

        public Results Result { get; set; }
        public float FinalScore { get; set; }
        public float Percentage { get; set; }
        public float FlooringUnit { get; set; }
        public int TotalQuestion { get; set; }
        public int WrongAns { get; set; }
        public int RightAns { get; set; }
        public int Attempted { get; set; }
        public Guid InPI { get; set; }
        public void Calculate()
        {
            RightAns = Result.Correct.Count(i => i == true);
            Attempted = Result.NotAttemp.Count;
            TotalQuestion = Result.Ans.Count;
            WrongAns = Result.Correct.Count(i => i == false) - (Result.NotAttemp.Count(i => i == true));



            Result.Exam.OnNoQuestion = Result.Exam.OnNoQuestion == 0 ? 1 : Result.Exam.OnNoQuestion;
            //check Negative Marking
            if (Result.Exam.IsNegativeMarking)
            { //Negative Marking
                //check Flooring Allowed
                if (Result.Exam.PercentageAllowed)
                {
                    //get the flooring amount.
                    FlooringUnit = Result.Exam.Negative / Result.Exam.OnNoQuestion;
                    FinalScore = RightAns - (WrongAns * FlooringUnit);

                }
                else
                {
                    FlooringUnit = Result.Exam.Negative;
                    int remainder = WrongAns / Result.Exam.OnNoQuestion;
                    FinalScore = RightAns - (remainder * FlooringUnit);
                }

            }
            else
            {

                //No negative Marking
                FinalScore = RightAns; ;


            }
            Percentage = (FinalScore / TotalQuestion) * 100;
        }
        public void Calculate(bool f = false)
        {




            Result.Exam.OnNoQuestion = Result.Exam.OnNoQuestion == 0 ? 1 : Result.Exam.OnNoQuestion;
            //check Negative Marking
            if (Result.Exam.IsNegativeMarking)
            { //Negative Marking
                //check Flooring Allowed
                if (Result.Exam.PercentageAllowed)
                {
                    //get the flooring amount.
                    FlooringUnit = Result.Exam.Negative / Result.Exam.OnNoQuestion;
                    FinalScore = RightAns - (WrongAns * FlooringUnit);

                }
                else
                {
                    FlooringUnit = Result.Exam.Negative;
                    int remainder = WrongAns / Result.Exam.OnNoQuestion;
                    FinalScore = RightAns - (remainder * FlooringUnit);
                }

            }
            else
            {

                //No negative Marking
                FinalScore = RightAns; ;


            }
            Percentage = (FinalScore / TotalQuestion) * 100;
        }

    }
}