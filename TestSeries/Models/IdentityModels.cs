﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace TestSeries.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [MaxLength(100)]
        [Required]
        [Display(Name="First Name")]
        public string FullName { get; set; }
        [MaxLength(400)]
        [Display(Name = "Current Institution Name")]
        public string TutionInstitueName { get; set; }
        [MaxLength(100)]
        [Display(Name="Preapering For Exam?")]
        public string PreparingFor { get; set; }
        
        [Display(Name="From ?")]
        public string City { get; set; }
        [Display(Name="Exam On?")]
        public int ExamOn { get; set; }

        public byte[] ProfileImage { get; set; }
        public bool HasImage { get; set; }
    }
    
}