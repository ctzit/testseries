﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class UnitOrChapter
    {
        public Guid Id { get; set; }
        
        public virtual Subject Subject { get; set; }
        [Display(Name=@"Unit/Chapter Name"),MinLength(3),MaxLength(200)]
        public string Name { get; set; }
        public DateTime AddedOn { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public UnitOrChapter()
        {
            if (AddedOn==null)
            {
                AddedOn = DateTime.UtcNow;
            }
        }
    }
}