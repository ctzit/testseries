﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSeries.Infra;

namespace TestSeries.Areas.QuestionBank.Controllers
{
    public class QuestionTypeController : Controller
    {
        //
        // GET: /QuestionBank/QuestionType/
        public ActionResult Index()
        {
            RadioEnumModel em = new RadioEnumModel();
            return View(em);
        }
	}
}