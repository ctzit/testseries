namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second_four : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "IsImportant", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "IsImportant");
        }
    }
}
