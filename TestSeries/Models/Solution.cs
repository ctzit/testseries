﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class Solution
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        [AllowHtml]
        public string Solved { get; set; }
        public bool Active { get; set; }
        public DateTime AddedOn { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public virtual Question Query { get; set; }
        public Solution()
        {
            if (Id == null)
            {
                Id = Guid.NewGuid();
            }
            if (AddedOn == null)
            {
                AddedOn = DateTime.UtcNow;
            }
        }
    }
}