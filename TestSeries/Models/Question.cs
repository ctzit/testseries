﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class Question
    {
        public Guid ID { get; set; }
        [AllowHtml]
        public string Query { get; set; }
        public int SubjectId { get; set; }
        // public virtual ICollection<Category> Categories { get; set; }
        public virtual Subject Subject { get; set; }
        public DateTime AddedOn { get; set; }
        public virtual ICollection<Answers> Answers { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string UserId { get; set; }
        public virtual Tenant Tenant { get; set; }
        [EnumDataType(typeof(QuestionType))]
        public QuestionType QuestionType { get; set; }
        [EnumDataType(typeof(DifficultyLevel))]
        public DifficultyLevel QuestionLevel { get; set; }
        public int QuestionWeightage { get; set; }
        [AllowHtml]
        public string Solution { get; set; }
        [Display(Name="Is question to be answered in fixed time bounds?")]
        public bool TimeBounded { get; set; }
        [Display(Name="Time Bound in minute.")]
        public int TimeBound { get; set; }
        public bool IsImportant { get; set; }
        public virtual ICollection<UnitOrChapter> UnitOrChapters { get; set; }
        public bool Published { get; set; }
        public Question()
        {
            if (ID == null)
            {
                ID = Guid.NewGuid();

            }
            if (AddedOn == null)
            {
                AddedOn = DateTime.UtcNow;
            }
            AddedOn = DateTime.UtcNow;
        }

    }

    public enum QuestionType
    {
        [Display(Name="Multiple Choice Question")]
        MCQ,
        [Display(Name = "Multiple Selection Question")]
        MSQ,
        [Display(Name = "Yes or No")]
        YES_NO,
        [Display(Name = "True or False")]
        TRUE_FALSE
        //[Display(Name = "Fill In The Blanks")]
        //FILL_IN_THE_BLANKS,
        //[Display(Name = "Image Based")]
        //IMAGE_BASED,
        //[Display(Name = "Hierarchial Ordering")]
        //HIERARCHIAL_ORDERING,
        //[Display(Name = "Matching")]
        //MATCHING,
        //[Display(Name = "Descriptive")]
        //DESCRIPTIVE


    }
    public enum DifficultyLevel
    {
        BEGINNER,
        MODERATE,
        HIGH
    }
}