namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class question_publ : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "Published", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "Published");
        }
    }
}
