﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class ImageForSite
    {
        public Guid Id { get; set; }
        public Byte[] Data { get; set; }
        public string Encoding { get; set; }
        public string User { get; set; }
        public string Agent { get; set; }
        public DateTime On { get; set; }
        public bool Active { get; set; }
        public string UserUrl { get; set; }
        public string UserHost { get; set; }
        public ImageForSite()
        {
            Id = Guid.NewGuid();
            On = DateTime.Now;
        
        }
        
    }
    //public class BackgroundImage {
    //    public Guid Id { get; set; }
    //    public Byte[] Data { get; set; }
    //    public string Encoding { get; set; }
    //    public string User { get; set; }
    //    public string Agent { get; set; }
    //    public DateTime On { get; set; }
    //    public bool AsHomePage { get; set; }
    //    public bool AsLoginPage { get; set; }
    //    public bool AsSignupPage { get; set; }
    //    public bool HasWaterMark { get; set; }
    //    public string WaterMarkText { get; set; }
    //    public bool Active { get; set; }
    //    public string UserIp { get; set; }
    //    public string UserHostName { get; set; }
    //    public BackgroundImage()
    //    {
    //        Id = Guid.NewGuid();
    //        On = DateTime.Now;
        
    //    }
    
    //}
    

}