﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace TestSeries.Models
{
    public class Set
    {
        public Guid SetId { get; set; }
        [Display(Name="Set Name"),Required,MaxLength(40),MinLength(3)]
        public string SetName { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual Exams Exam { get; set; }
    }
}
