﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSeries.Models;

namespace TestSeries.Infra
{
    public class App : Controller
    {
        //Here default means , direct website has been hit.
        internal string TenantUrl = "Default";
        internal string TenantHost = "Default";
        internal bool AuthoriziedTenant = false;
        internal User UserDetails;
        internal TestSeriesContext dc = new TestSeriesContext();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
#if DEBUG
            AuthoriziedTenant = true;
#endif
            TenantUrl = filterContext.HttpContext.Request.Url.ToString();
            TenantHost = filterContext.HttpContext.Request.Url.Host.ToString();
            if (dc.Tenants.Count(i => i.TenantUrl == TenantUrl) > 0)
            {
               var Tenant= dc.Tenants.Find(TenantUrl);
               if (Tenant.Active && Tenant.ExpiresOn > DateTime.UtcNow)
                   AuthoriziedTenant = true;
            }

            if (User.Identity.IsAuthenticated)
            {
                var uid = User.Identity.Name;
                UserDetails = dc.Users.FirstOrDefault(i=>i.UserName==uid);
            }
            base.OnActionExecuting(filterContext);
        }

        internal void Sucess(string Message)
        {
            TempData["Success"] = Message;
        }
        internal void Error(string Message)
        {
            TempData["Error"] = Message;
        }
        internal void Info(string Message)
        {
            TempData["Info"] = Message;
        }
    }
}