﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSeries.Models
{
    public class ExamSubjects
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public int SubjectId { get; set; }
        public int NoOfQuestion { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public virtual Exams Exams { get; set; }
        //public virtual Subject Subject { get; set; }
        public string ShowName { get; set; }

    }
}