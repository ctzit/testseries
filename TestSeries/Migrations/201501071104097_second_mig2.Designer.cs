// <auto-generated />
namespace TestSeries.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class second_mig2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(second_mig2));
        
        string IMigrationMetadata.Id
        {
            get { return "201501071104097_second_mig2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
