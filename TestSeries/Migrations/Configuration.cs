namespace TestSeries.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TestSeries.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TestSeries.Models.TestSeriesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TestSeries.Models.TestSeriesContext context)
        {
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);

                var role = new IdentityRole { Name = "AppAdmin" };
                if (!context.Roles.Any(r => r.Name == "AppAdmin"))
                    manager.Create(role);

                var role1 = new IdentityRole { Name = "SuperAdmin" };
                if (!context.Roles.Any(r => r.Name == "SuperAdmin"))
                    manager.Create(role1);

                var role3 = new IdentityRole { Name = "SysAdmin" };
                if (!context.Roles.Any(r => r.Name == "SysAdmin"))
                    manager.Create(role3);

                var role4 = new IdentityRole { Name = "Student" };
                if (!context.Roles.Any(r => r.Name == "Student"))
                    manager.Create(role4);
            }
            try
            {
                var store1 = new UserStore<User>(context);
                var manager1 = new UserManager<User>(store1);
                if (!context.Users.Any(i => i.UserName == "developer"))
                {
                    var user = new User { UserName = "developer",FullName="developer",PhoneNumber="1111111111"};
                    manager1.Create(user, "!Qaz2wsx");
                    manager1.AddToRole(user.Id, "SuperAdmin");
                }
                if (!context.Users.Any(i => i.UserName == "student"))
                {
                    var user = new User { UserName = "student", FullName = "developer",PhoneNumber="1111111112" };
                    manager1.Create(user, "!Qaz2wsx");
                    manager1.AddToRole(user.Id, "Student");
                }
                if (!context.Users.Any(i => i.UserName == "admin"))
                {
                    var user = new User { UserName = "admin", FullName = "developer",  PhoneNumber = "1111111113" };
                    manager1.Create(user, "!Qaz2wsx");
                    manager1.AddToRole(user.Id, "AppAdmin");
                }
                if (!context.Users.Any(i => i.UserName == "sysadmin"))
                {
                    var user = new User { UserName = "sysadmin", FullName = "developer", PhoneNumber="1111111114" };
                    manager1.Create(user, "!Qaz2wsx");
                    manager1.AddToRole(user.Id, "SysAdmin");
                }
            }
            catch(Exception e)
            {
                Console.Write(e.Data);
            }
        }
    }
}
