namespace TestSeries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exams_changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "TotalQuestion", c => c.Int(nullable: false));
            DropColumn("dbo.Exams", "Details");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Exams", "Details", c => c.String());
            DropColumn("dbo.Exams", "TotalQuestion");
        }
    }
}
