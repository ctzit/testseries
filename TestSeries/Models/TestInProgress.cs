﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSeries.Models
{
    public class TestInProgress
    {
        public Guid Id { get; set; }
        public string User { get; set; }
        public int TotalQ { get; set; }
        public int RightA { get; set; }
        public int ExamId { get; set; }
        public bool Over { get; set; }
        public virtual ICollection<AnsweredBack> AnswersGiven { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public bool IsPaused { get; set; }
        /// <summary>
        /// If Exam Was paused or abruppted then it will store time left.
        /// </summary>
        public int TimeRemaining { get; set; }

    }
}