﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TestSeries.Models;

namespace TestSeries.Controllers
{
    public class QuesController : ApiController
    {
        private TestSeriesContext db = new TestSeriesContext();
        public QuesController()
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            
        }

        // GET: api/Ques
        public IQueryable<Question> GetQuestions()
        {
            return db.Questions;
        }

        // GET: api/Ques/5
        [ResponseType(typeof(Question))]
        public IHttpActionResult GetQuestion(Guid id)
        {
            Question question = db.Questions.Find(id);
            

            if (question == null)
            {
                return NotFound();
            }
            //Load All Navigation Explicitly
            db.Entry(question).Collection(i => i.Answers)
                .Load();
            //db.Entry(question).Collection(i => i.Answers);
               
            db.Entry(question).Collection(i => i.UnitOrChapters)
                .Load();
            question.Answers.OrderBy(i => i.Placement);
            //db.Entry(question).Reference(i => i.Subject).Load();
            question.Answers = question.Answers.OrderBy(i => i.Placement).ToList();
            return Ok(question);
        }

        // PUT: api/Ques/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutQuestion(Guid id, Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.ID)
            {
                return BadRequest();
            }
            List<UnitOrChapter> Units = question.UnitOrChapters.ToList();
            question.Subject = null;
            question.UnitOrChapters= new List<UnitOrChapter>();
            db.Entry(question).State = EntityState.Modified;
            

            try
            {
                db.SaveChanges();
                db.Entry(question).Collection(i => i.UnitOrChapters).Load();
                db.Configuration.LazyLoadingEnabled = true;
                db.Configuration.ProxyCreationEnabled = true;
                question.UnitOrChapters.Clear();
                db.SaveChanges();
                foreach (var item in Units)
                {
                    var d = db.UnitOrChapter.Find(item.Id);
                    if (!d.Questions.Any(i => i.ID == question.ID))
                    {
                        d.Questions.Add(question);
                        db.Entry(d).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                   
                }
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ques
        [ResponseType(typeof(Question))]
        public IHttpActionResult PostQuestion(Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<UnitOrChapter> Units = question.UnitOrChapters.ToList();
            question.Subject = null;
            question.UnitOrChapters = null;
            question.Subject = null;
            //List<UnitOrChapter> Units
            db.Questions.Add(question);

            try
            {
                db.SaveChanges();
               
                db.Configuration.LazyLoadingEnabled = true;
                db.Configuration.ProxyCreationEnabled = true;
                foreach (var item in Units)
                {
                    var d = db.UnitOrChapter.Find(item.Id);
                    if (!d.Questions.Any(i => i.ID == question.ID))
                    {
                        d.Questions.Add(question);
                        db.Entry(d).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                }
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
            }
            catch (DbUpdateException)
            {
                if (QuestionExists(question.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = question.ID }, question);
        }

        // DELETE: api/Ques/5
        [ResponseType(typeof(Question))]
        public IHttpActionResult DeleteQuestion(Guid id)
        {
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return NotFound();
            }

            db.Questions.Remove(question);
            db.SaveChanges();

            return Ok(question);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuestionExists(Guid id)
        {
            return db.Questions.Count(e => e.ID == id) > 0;
        }
    }
}